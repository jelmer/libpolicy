/* 
   Unix SMB/CIFS implementation.
   Copyright (C) 2006 Wilco Baan Hofman <wilco@baanhofman.nl>
   Copyright (C) 2006 Jelmer Vernooij <jelmer@samba.org>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   For more information on the .ADM file format:
   http://msdn2.microsoft.com/en-us/library/aa372405.aspx 
*/

%{
#include "config.h"
void error_message (const char *format, ...);
int yyparse (void);
void yyerror (const char *s);
extern int yylex (void);

%}

%union {
	char *text;
	int integer;
}

%token CATEGORY
%token CLASS
%token CLASS_USER
%token CLASS_MACHINE
%token POLICY
%token KEYNAME
%token EXPLAIN
%token VALUENAME
%token VALUEON VALUEOFF
%token PART
%token ITEMLIST
%token NAME
%token VALUE VALUEPREFIX
%token NUMERIC EDITTEXT TEXT DROPDOWNLIST CHECKBOX LISTBOX
%token MINIMUM MAXIMUM DEFAULT
%token END
%token ACTIONLIST ACTIONLISTON ACTIONLISTOFF
%token EXPLICITVALUE
%token DEL
%token SUPPORTED
%token <text> LITERAL
%token <integer> INTEGER
%token <text> LOOKUPLITERAL
%token CLIENTEXT
%token REQUIRED
%token NOSORT ADDITIVE
%token MAXLEN
%token SPIN
%token <text> EQUALS
%token <text> SECTION
%token <text> IF
%token ENDIF
%token TXTCONVERT
%token DEFCHECKED EXPANDABLETEXT

%start admfile

%% 

admfile: classes sections;

classes: /* empty */ | class classes;

class: CLASS classvalue categories;
classvalue: CLASS_USER|CLASS_MACHINE;

categories: /* empty */ | category categories;

string: LITERAL | LOOKUPLITERAL;

category: CATEGORY string categoryitems END CATEGORY;

categoryitem: explain | category | policy | keyname;
categoryitems: categoryitem categoryitems | /* empty */ ;

policy: POLICY string policyitems END POLICY;
policyitem: explain | keyname | valuename | valueon | valueoff | min | max | defaultvalue | supported | part | DEFCHECKED | actionliston | actionlistoff | EXPLICITVALUE | valueprefix | clientext | NOSORT | ADDITIVE | maxlen | EXPANDABLETEXT | TXTCONVERT;
policyitems: policyitem policyitems | /* empty */;

valuetype: NUMERIC | EDITTEXT | TEXT | DROPDOWNLIST | CHECKBOX | LISTBOX;

part: PART string valuetype partitems END PART;

spin: SPIN INTEGER;

partitem: keyname | valuename | valueon | valueoff | min | max | defaultvalue | itemlist | REQUIRED | spin | DEFCHECKED | actionliston | actionlistoff | EXPLICITVALUE | valueprefix | clientext | NOSORT | ADDITIVE | maxlen | EXPANDABLETEXT |TXTCONVERT;
partitems: partitem partitems | /* empty */;

clientext: CLIENTEXT LITERAL;
maxlen: MAXLEN INTEGER;
min: MINIMUM INTEGER;
max: MAXIMUM INTEGER;
defaultvalue: DEFAULT INTEGER | DEFAULT LOOKUPLITERAL | DEFAULT LITERAL;

explain: EXPLAIN string;
value: DEL | INTEGER | NUMERIC INTEGER | LITERAL | LOOKUPLITERAL;

valueon: VALUEON value;
valueoff: VALUEOFF value;
valueprefix: VALUEPREFIX LITERAL;

valuename: VALUENAME string;
keyname: KEYNAME string;

itemlist: ITEMLIST items END ITEMLIST;
itemname: NAME string;
itemvalue: VALUE value;

item: itemname | itemvalue | DEFAULT | actionlist;
items: /* empty */ | item items;

supported: SUPPORTED string;

actionlist: ACTIONLIST actions END ACTIONLIST;
actionliston: ACTIONLISTON actions END ACTIONLISTON; 
actionlistoff: ACTIONLISTOFF actions END ACTIONLISTOFF; 
actions: keyname actions | valuename actions | itemvalue actions | /* empty */;

variable: LITERAL EQUALS;
variables: variable variables | /* empty */;
sections: section sections | /* empty */;
section: SECTION variables;

%%

void
yyerror (const char *s)
{
     error_message ("%s\n", s);
}



