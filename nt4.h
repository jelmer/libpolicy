/* 
   Unix SMB/CIFS implementation.
   Client-side (group) policy management library.
   
   Copyright (C) Wilco Baan Hofman <wilco@baanhofman.nl> 2008
   Copyright (C) Jelmer Vernooij <jelmer@samba.org> 2008
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _NT4_H_
#define _NT4_H_


struct nt4_policy_context {

};


/* Policy creation (NT4 specific) */
WERROR pol_nt4_create_policy(struct dom_sid domain, struct nt4_policy_context **pol_ctx);


/* Policy manipulation */
WERROR pol_nt4_open_policy(struct policy_context *pol_ctx);
WERROR pol_nt4_close_policy(struct policy_context *pol_ctx);

enum pol_nt4_target {
	POL_TARGET_USER,
	POL_TARGET_USERGROUP,
	POL_TARGET_MACHINE,
};
WERROR pol_nt4_list_policy_settings(struct nt4_policy_context *pol_ctx);
WERROR pol_nt4_get_policy_setting(struct nt4_policy_context *pol_ctx, const char *setting, uint32_t data_type, DATA_BLOB *value);
WERROR pol_nt4_set_policy_setting(struct nt4_policy_context *pol_ctx, const char *setting, uint32_t data_type, DATA_BLOB value);

#endif /* _NT4_H_ */