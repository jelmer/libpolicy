/* 
   Unix SMB/CIFS implementation.
   Samba utility functions
   Copyright (C) Jelmer Vernooij <jelmer@samba.org> 2009
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include <talloc.h>
#include "adm.h"

typedef struct {
	PyObject_HEAD
	struct adm_file *file;
} AdmFileObject;

static int adm_file_init(PyObject *self, PyObject *args, PyObject *kwargs)
{
	char *path;
	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "s", &path))
		return -1;

	adm_read_file(path);

	return 0;
}

static void adm_file_dealloc(AdmFileObject *self)
{
	talloc_free(self->file);
	self->ob_type->tp_free(self);
}

static PyTypeObject PyAdmFile = {
	.tp_name = "AdmFile",
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_basicsize = sizeof(AdmFileObject),
	.tp_dealloc = (destructor)adm_file_dealloc,
	.tp_init = adm_file_init,
};

void initpolicy(void)
{
	PyObject *m;

	if (PyType_Ready(&PyAdmFile) < 0)
		return;
	
	m = Py_InitModule3("policy", NULL, "Windows policy management");
	if (m == NULL)
		return;

	PyModule_AddObject(m, "AdmFile", (PyObject *)&PyAdmFile);
	Py_INCREF(&PyAdmFile);
}
