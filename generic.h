/* 
   Unix SMB/CIFS implementation.
   Client-side (group) policy management library.
   
   Copyright (C) Wilco Baan Hofman <wilco@baanhofman.nl> 2008
   Copyright (C) Jelmer Vernooij <jelmer@samba.org> 2008
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _GENERIC_H_
#define _GENERIC_H_

/** Generic Policy API */

struct policy_context {
	union { 
		struct group_policy_context *group_pol_ctx;
		struct nt4_policy_context *nt4_pol_ctx;
	};
};


/*
  Create GPO (Group Policy Object?)
* create groupPolicyContainer

Create GPT (Group Policy Template)
* create empty tree on SYSVOL
* generate GPT.INI

Consistent generation of Security Descriptors and Version number in GPO and GPT

apply policy should have some of the samba3 internals?

API should be so nicely abstracted, that samba3 can plugin its own CSEs.
*/

enum pol_apply_target {
	POL_APPLY_USER,
	POL_APPLY_MACHINE,
	POL_APPLY_BOTH,
};
enum pol_apply_source {
	POL_SRC_AD,
	POL_SRC_NT4,
	POL_SRC_9x,
};

WERROR pol_apply_policy(struct registry_context *reg, struct dom_sid domain_sid, 
		enum pol_apply_target, struct dom_sid user_sid,
		struct dom_sid machine_sid, const char *username, 
		const char *machinename, enum pol_apply_source);




/* Apply policy (registry context, domain?, USER/MACHINE/BOTH, user (sid?), machine(sid?), LOAD_NT4/LOAD_AD)
- Get list of (relevant) GPO objects
- Apply GPO objects to registry context
- Call Client side extensions function

Create policy ()
In case of NT4:
- Create empty NTConfig.pol
In case of AD:
- Create GPO
- Create GPT

Set group policy for DN ()
- Add LDAP attributes to the DN

Open policy (domain?, UUID?)
- Open a GPO/NTConfig.pol

Set policy setting (policy fd)
- Set a setting in the policy file 

List policy settings(policy fd)
- Get list of all settings in the opened policy file

*/

#endif /* _GENERIC_H_ */