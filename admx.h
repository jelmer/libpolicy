/* 
   Unix SMB/CIFS implementation.
   Client-side (group) policy management library.
   
   Copyright (C) Wilco Baan Hofman <wilco@baanhofman.nl> 2008
   Copyright (C) Jelmer Vernooij <jelmer@samba.org> 2008
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _ADMX_H_
#define _ADMX_H_

struct admx_file {

};

struct admx_file *admx_read_file(const char *filename);

#endif /* _ADMX_H_ */