/* 
   Unix SMB/CIFS implementation.
   Client-side (group) policy management library.
   
   Copyright (C) Wilco Baan Hofman <wilco@baanhofman.nl> 2008
   Copyright (C) Jelmer Vernooij <jelmer@samba.org> 2008
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _GPO_H_
#define _GPO_H_

#include <ldb.h>

struct group_policy_context {

};

WERROR pol_ad_init(struct dom_sid domain, struct group_policy_context **pol_ctx);

/* Policy creation (AD specific) */
WERROR pol_ad_create_policy(struct group_policy_context *pol_ctx, struct GUID **guid);
 

WERROR pol_ad_set_policies_for_dn(struct group_policy_context *pol_ctx, struct ldb_dn *dn, 
                                struct GUID **policies);
WERROR pol_ad_list_policies_for_dn(struct group_policy_context *pol_ctx, struct ldb_dn *dn,
                                struct GUID **policies);

struct policy_setting {
	const char *setting;
	uint32_t data_type;
	DATA_BLOB value;
};

WERROR pol_ad_list_policy_settings(struct group_policy_context *pol_ctx, struct policy_setting **settings); 
WERROR pol_ad_get_policy_setting(struct group_policy_context *pol_ctx, const char *req_setting, struct policy_setting *setting);
WERROR pol_ad_set_policy_setting(struct group_policy_context *pol_ctx, struct policy_setting *setting);

#endif /* _GPO_H_ */