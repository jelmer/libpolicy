.DEFAULT_GOAL = all

configure: configure.ac
	./autogen.sh

config.status: configure
	./configure

config.mk: config.status
	./config.status

include config.mk

all:: dumpadm

parse_adm.c parse_adm.h: parse_adm.y
	$(YACC) -d $< -o $@

lex.yy.c lex.h: lex.l
	$(LEX) -i --header-file=lex.h $< 

lex.yy.o: lex.yy.c parse_adm.h

parse_adm.o lex.yy.o: CFLAGS+=-fPIC

libpolicy.$(SHLIBEXT): parse_adm.o lex.yy.o
	$(CC) -shared -o $@ $^ $(TALLOC_LIBS)

dumpadm: dumpadm.o libpolicy.$(SHLIBEXT)
	$(CC) -o $@ $^ $(POPT_LIBS)

clean::
	rm -f dumpadm
	rm -f *.o
	rm -f parse_adm.c parse_adm.h
	rm -f lex.yy.c

distclean::
	rm -f config.mk
	rm -rf autom4te.cache/
	rm -f config.h

python:: python/policy.$(SHLIBEXT)

check-python:: python
	PYTHONPATH=python $(TRIAL) python/tests

python/policy.$(SHLIBEXT): pypolicy.o libpolicy.$(SHLIBEXT)
	$(CC) -shared -o $@ $^ `$(PYTHON_CONFIG) --libs` $(TALLOC_LIBS)

pypolicy.o: CFLAGS+=`$(PYTHON_CONFIG) --cflags` -fPIC
